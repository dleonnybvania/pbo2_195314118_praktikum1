/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

import java.util.Scanner;

public class Main_KoleksiPerpustakaan {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int jumlah = 0;
        int denda = 0;
        String nm, nama, alamat, nim, nip, ktp;
        String jenis, id_koleksi, judul, penerbit, thTerbit;
        String isbn, jumHalaman, format, issn, volume, series;
        
        System.out.println("Peminjam dibedakan "
                + "menjadi tiga jenis, antara lain:");
        System.out.println("1. Mahasiswa");
        System.out.println("2. Dosen");
        System.out.println("3. Masyarakat Umum");
        System.out.println("Jika Anda merupakan Mahasiswa, "
                + "maka tuliskan 'M'");
        System.out.println("Jika Anda merupakan Dosen, "
                + "maka tuliskan 'D'");
        System.out.println("Jika Anda bukan Mahasiswa "
                + "maupun Dosen di Universitas "
                + "ini maka tuliskan 'U'");
        System.out.println("");
        System.out.println("");
        
        System.out.print("Masukkan Jumlah Peminjam : ");
        int jum = sc.nextInt();
        
        Peminjam[] pem = new Peminjam[jum];
        KoleksiPerpustakaan[] kp = new KoleksiPerpustakaan[jum];
        
        
        for (int i = 0; i < pem.length; i++) {

            System.out.println("DATA PEMINJAM KE- "+(i+1));
           

            System.out.print("Nama Peminjam    :");
            nm = sc.next();
            
            if("M".equals(nm)){
                
                System.out.print("Masukkan Nama     :");
                nama = sc.next();
                
                System.out.print("Masukkan Alamat   :");
                alamat = sc.next();
                
                System.out.print("Masukkan NIM      :");
                nim = sc.next();
          
                pem[i] = new Mahasiswa(nama, alamat, nim);
                
                
                System.out.println("Jenis Koleksi yang Dipinjam:");
                jenis = sc.next();
                
                 if("buku".equals(jenis)){
                   
                System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISBN             :");
                isbn = sc.next();
                
                System.out.print("Jumlah Halaman            :");
                jumHalaman = sc.next();
                
                kp[i] = new Buku(id_koleksi, judul, penerbit, thTerbit, isbn, jumHalaman);

                }
                else if("cd/dvd".equals(jenis)){
                   
                System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISBN             :");
                isbn = sc.next();
                
                System.out.print("Format                    :");
                format = sc.next();
                
                kp[i] = new CD_DVD(id_koleksi, judul, penerbit, thTerbit, isbn, format);

                }
                
                else if("majalah".equals(jenis)){
                    System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISSN             :");
                issn = sc.next();
                
                System.out.print("Masukkan Volume           :");
                volume = sc.next();
                
                System.out.print("Masukkan Series           :");
                series = sc.next();
                
                kp[i] = new Majalah(id_koleksi, judul, penerbit, thTerbit, issn, volume, series);
                }
 
                }
            
            else if("D".equals(nm)){
                
                System.out.print("Masukkan Nama     :");
                nama = sc.next();
                
                System.out.print("Masukkan Alamat   :");
                alamat = sc.next();
                
                System.out.print("Masukkan NIP      :");
                nip = sc.next();
          
                pem[i] = new Dosen(nama, alamat, nip);
                
                
                System.out.println("Jenis Koleksi yang Dipinjam:");
                jenis = sc.next();
                
                 if("buku".equals(jenis)){
                   
                System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISBN             :");
                isbn = sc.next();
                
                System.out.print("Jumlah Halaman            :");
                jumHalaman = sc.next();
                
                kp[i] = new Buku(id_koleksi, judul, penerbit, thTerbit, isbn, jumHalaman);

                }
                else if("cd/dvd".equals(jenis)){
                   
                System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISBN             :");
                isbn = sc.next();
                
                System.out.print("Format                    :");
                format = sc.next();
                
                kp[i] = new CD_DVD(id_koleksi, judul, penerbit, thTerbit, isbn, format);

                }
                
                else if("majalah".equals(jenis)){
                    System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISSN             :");
                issn = sc.next();
                
                System.out.print("Masukkan Volume           :");
                volume = sc.next();
                
                System.out.print("Masukkan Series           :");
                series = sc.next();
                
                kp[i] = new Majalah(id_koleksi, judul, penerbit, thTerbit, issn, volume, series);
                }
 
                }
            
            else if("U".equals(nm)){
                
                System.out.print("Masukkan Nama     :");
                nama = sc.next();
                
                System.out.print("Masukkan Alamat   :");
                alamat = sc.next();
                
                System.out.print("Masukkan KTP      :");
                ktp = sc.next();
          
                pem[i] = new MUmum(nama, alamat, ktp);
                
                
                System.out.println("Jenis Koleksi yang Dipinjam:");
                jenis = sc.next();
                
                 if("buku".equals(jenis)){
                   
                System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISBN             :");
                isbn = sc.next();
                
                System.out.print("Jumlah Halaman            :");
                jumHalaman = sc.next();
                
                kp[i] = new Buku(id_koleksi, judul, penerbit, thTerbit, isbn, jumHalaman);

                }
                else if("cd/dvd".equals(jenis)){
                   
                System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISBN             :");
                isbn = sc.next();
                
                System.out.print("Format                    :");
                format = sc.next();
                
                kp[i] = new CD_DVD(id_koleksi, judul, penerbit, thTerbit, isbn, format);

                }
                
                else if("majalah".equals(jenis)){
                    System.out.print("Masukkan ID_KOLEKSI       :");
                id_koleksi = sc.next();
                
                System.out.print("Masukkan Judul            :");
                judul = sc.next();
                
                System.out.print("Masukkan Nama Penerbit    :");
                penerbit = sc.next();
                
                System.out.print("Masukkan Tahun Terbit     :");
                thTerbit = sc.next();
                
                System.out.print("Masukkan ISSN             :");
                issn = sc.next();
                
                System.out.print("Masukkan Volume           :");
                volume = sc.next();
                
                System.out.print("Masukkan Series           :");
                series = sc.next();
                
                kp[i] = new Majalah(id_koleksi, judul, penerbit, thTerbit, issn, volume, series);
                }
 
                }
            
            pem[i].DendaPerpus(denda);
            
        }
        
    }
}

  


                
               
                

            
            
