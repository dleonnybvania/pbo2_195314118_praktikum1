/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

/**
 *
 * @author SAMSUNG
 */
public class Majalah extends KoleksiPerpustakaan {
    private String issn;
    private String volume;
    private String series;

    public Majalah(String id_koleksi, String judul, String penerbit, String thTerbit, String issn, String volume, String series) {
        super(id_koleksi,judul,penerbit,thTerbit);
        this.issn = issn;
        this.volume = volume;
        this.series = series;
    }
    
    public void setISSN(String issn){
        this.issn = issn;
    }
    public String getISSN(){
        return issn;
    }
    
    public void setVOLUME(String volume){
        this.volume = volume;
    }
    public String getVOLUME(){
        return volume;
    }
    
    public void setSERIES(String series){
        this.series = series;
    }
    public String getSERIES(){
        return series;
    }

    
}
