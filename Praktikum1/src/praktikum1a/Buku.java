/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

/**
 *
 * @author SAMSUNG
 */
public class Buku extends KoleksiPerpustakaan {
    private String isbn;
    private String jumHalaman;

    public Buku(String id_koleksi, String judul, String penerbit, String thTerbit, String isbn, String jumHalaman) {
        super(id_koleksi,judul,penerbit,thTerbit);
        this.isbn = isbn;
        this.jumHalaman = jumHalaman;
    }
    
    public void setISBN(String isbn){
        this.isbn = isbn;
    }
    public String getISBN(){
        return isbn;
    }
    
    public void setJUMHAL(String jumHalaman){
        this.jumHalaman = jumHalaman;
    }
    public String getJUMHAL(){
        return jumHalaman;
    }
    
    
}
