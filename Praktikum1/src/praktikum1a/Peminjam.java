/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

import java.util.Scanner;

public class Peminjam {
    private String nama;
    private String alamat;
    
    Scanner sc = new Scanner(System.in);

    public Peminjam(String nama, String alamat) {
        this.nama = nama;
        this.alamat = alamat;
    }
    
    public void setNAMA(String nama){
        this.nama = nama;
    }
    public String getNAMA(){
        return nama;
    }
    
    public void setALAMAT(String alamat){
        this.alamat = alamat;
    }
    public String getALAMAT(){
        return alamat;
    }
    
    public int DendaPerpus(int denda){
        int maks_hari_pinjam = 7;
        int denda_per_hari = 500;
        int lama_pinjam;
        int jumKoleksi;
        
        System.out.print("Jumlah Koleksi yang Dipinjam    :");
        jumKoleksi = sc.nextInt();
        System.out.print("Sudah Berapa Hari Pinjam        ?");
        lama_pinjam = sc.nextInt();
        
        if(lama_pinjam < maks_hari_pinjam || lama_pinjam == maks_hari_pinjam){
            System.out.println("Peminjam Tidak Perlu Membayar Denda");
        } else if(lama_pinjam > maks_hari_pinjam){
            denda = (lama_pinjam-maks_hari_pinjam)*denda_per_hari*jumKoleksi;
        }
        return denda;
    }

}
