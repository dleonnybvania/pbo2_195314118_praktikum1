/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

/**
 *
 * @author SAMSUNG
 */
public class CD_DVD extends KoleksiPerpustakaan {
    private String isbn;
    private String format;

    public CD_DVD(String id_koleksi, String judul, String penerbit, String thTerbit, String isbn, String format) {
        super(id_koleksi,judul,penerbit,thTerbit);
        this.isbn = isbn;
        this.format = format;
    }
    
    public void setISBN(String isbn){
        this.isbn = isbn;
    }
    public String getISBN(){
        return isbn;
    }
    
    public void setFORMAT(String format){
        this.format = format;
    }
    public String getFORMAT(){
        return format;
    }

}

