/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

/**
 *
 * @author SAMSUNG
 */
public class MUmum extends Peminjam {
    private String ktp;

    public MUmum(String nama, String alamat, String ktp) {
        super(nama, alamat);
        this.ktp = ktp;
    }
    
    public void setKTP(String ktp){
        this.ktp = ktp;
    }
    public String getKTP(){
        return ktp;
    }
    
    
}
