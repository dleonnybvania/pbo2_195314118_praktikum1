/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

/**
 *
 * @author SAMSUNG
 */
public class Dosen extends Peminjam {
    private String nip;

    public Dosen(String nama, String alamat, String nip) {
        super(nama, alamat);
        this.nip = nip;
    }
    
    public void setNIP(String nip){
        this.nip = nip;
    }
    public String getNIP(){
        return nip;
    }
    
    
    
}
