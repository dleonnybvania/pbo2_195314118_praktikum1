/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

/**
 *
 * @author SAMSUNG
 */
public class Mahasiswa extends Peminjam {
    private String nim;

    public Mahasiswa(String nama, String alamat, String nim) {
        super(nama, alamat);
        this.nim = nim;
    }
    
    public void setNIM(String nim){
        this.nim = nim;
    }
    public String getNIM(){
        return nim;
    }
    
    
    
}
