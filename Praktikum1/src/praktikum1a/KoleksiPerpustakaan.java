/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum1a;

/**
 *
 * @author SAMSUNG
 */
public class KoleksiPerpustakaan {
    private String id_koleksi;
    private String judul;
    private String penerbit;
    private String thTerbit;
    

    public KoleksiPerpustakaan(String id_koleksi, String judul, String penerbit, String thTerbit) {
        this.id_koleksi = id_koleksi;
        this.judul = judul;
        this.penerbit = penerbit;
        this.thTerbit = thTerbit;
        
    }
    
    public void setID(String id_koleksi){
        this.id_koleksi = id_koleksi;
    }
    public String getID(){
        return id_koleksi;
    }
    
    public void setJUDUL(String judul){
        this.judul = judul;
    }
    public String getJUDUL(){
        return judul;
    }
    
    public void setPENERBIT(String penerbit){
        this.penerbit = penerbit;
    }
    public String getPENERBIT(){
        return penerbit;
    }
    
    public void setTH(String thTerbit){
        this.thTerbit = thTerbit;
    }
    public String getTH(){
        return thTerbit;
    }
    
   
}

