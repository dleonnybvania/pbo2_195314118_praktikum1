/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Praktikum1B;

/**
 *
 * @author SAMSUNG
 */
public class MasyarakatSekitar extends Penduduk {
    private String dataNomor;

    public MasyarakatSekitar() {
    }

    public MasyarakatSekitar(String dataNomor, String dataNama, String dataTempatTanggalLahir) {
        super(dataNama,dataTempatTanggalLahir);
        this.dataNomor = dataNomor;
    }

    public void setNomor(String dataNomor) {
        this.dataNomor = dataNomor;
    }
    public String getNomor() {
        return dataNomor;
    }

    @Override
    public double hitungIuran() {
        double iuran = Double.parseDouble(dataNomor);
        return iuran * 100;
    }

    @Override
    public String toString() {
        return dataNama+"\t"+dataTempatTanggalLahir+"\t"+dataNomor+"\t\tRp "+hitungIuran();
    }
    
    
    
}

