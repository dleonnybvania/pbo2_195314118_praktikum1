/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Praktikum1B;

/**
 *
 * @author SAMSUNG
 */
public class Main {
    public static void main(String[] args) {
        
        //data ukm
        UKM ukm = new UKM();    
        ukm.setNamaUnit("Tenis");
        
        //ketua dan sekretaris 
        Mahasiswa ketua = new Mahasiswa("195314118", "Leo", "24/03/2001");
        Mahasiswa sekretaris = new Mahasiswa("195314420", "Laras", "03/10/2000");
        
        //anggota
        MasyarakatSekitar anggota1 = new MasyarakatSekitar("212", "Yudi", "18/07/1998");
        MasyarakatSekitar anggota2 = new MasyarakatSekitar("195", "Suparni", "02/12/1999");
        Mahasiswa anggota3 = new Mahasiswa("185314111", "Mike", "03/02/1999");
        Mahasiswa anggota4 = new Mahasiswa("195314107", "Budi", "13/08/2001");
        
        Penduduk[] dataAnggota = new Penduduk[6];
        dataAnggota[0] = ketua;
        dataAnggota[1] = sekretaris;
        dataAnggota[2] = anggota1;
        dataAnggota[3] = anggota2;
        dataAnggota[4] = anggota3;
        dataAnggota[5] = anggota4;
        
        //tampil data
        System.out.println("UKM     : "+ukm.getNamaUnit());
        System.out.println();
        
        System.out.println("Ketua UKM");
        System.out.println("Nama            : "+ketua.getNama());
        System.out.println("NIM             : "+ketua.getNim());
        System.out.println("Tanggal Lahir   : "+ketua.getdataTempatTanggalLahir());
        System.out.println("Iuran           : Rp "+ketua.hitungIuran());
        System.out.println();
        
        System.out.println("Sekrearis UKM");
        System.out.println("Nama            : "+sekretaris.getNama());
        System.out.println("NIM             : "+sekretaris.getNim());
        System.out.println("Tanggal Lahir   : "+sekretaris.getdataTempatTanggalLahir());
        System.out.println("Iuran           : Rp "+sekretaris.hitungIuran());
        System.out.println();
        
        System.out.println("Daftar anggota");
        System.out.println();
        System.out.println("================================================================");
        System.out.println("NO\tNAMA\tTGL LAHIR\tNIM/NO\t\tIURAN");
        System.out.println("================================================================");
        
        int totalIuran = 0;
        for (int i = 2; i < dataAnggota.length; i++) {
            System.out.println(i-1+"\t"+dataAnggota[i].toString());
            totalIuran += dataAnggota[i].hitungIuran();
        }
        
        System.out.println("================================================================");
        totalIuran = (int) (totalIuran + (ketua.hitungIuran() + sekretaris.hitungIuran()));
        System.out.println("Total iuran         : Rp "+totalIuran);
    }
    
}
