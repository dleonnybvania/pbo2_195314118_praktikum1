/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Praktikum1B;

/**
 *
 * @author SAMSUNG
 */
public class Mahasiswa extends Penduduk {
    private String dataNim;

    public Mahasiswa() {
    }

    public Mahasiswa(String dataNim, String dataNama, String dataTempatTanggalLahir) {
        super(dataNama,dataTempatTanggalLahir);
        this.dataNim = dataNim;
    }

    
    public void setNim(String dataNim) {
        this.dataNim = dataNim;
    }
    public String getNim() {
        return dataNim;
    }
    
    @Override
    public double hitungIuran() {
        double iuran = Double.parseDouble(dataNim);
        return Math.round(iuran/10000);
    }
    
    @Override
    public String toString() {
        return dataNama+"\t"+dataTempatTanggalLahir+"\t"+dataNim+"\tRp "+hitungIuran();
    }
    


}
