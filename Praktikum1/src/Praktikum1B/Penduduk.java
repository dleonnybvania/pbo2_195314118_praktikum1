/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Praktikum1B;

/**
 *
 * @author SAMSUNG
 */
public abstract class Penduduk {
    protected String dataNama;
    protected String dataTempatTanggalLahir;

    public Penduduk() {
    }

    public Penduduk(String dataNama, String dataTempatTanggalLahir) {
        this.dataNama = dataNama;
        this.dataTempatTanggalLahir = dataTempatTanggalLahir;
    }

    
    public void setNama(String dataNama) {
        this.dataNama = dataNama;
    }
    public String getNama() {
        return dataNama;
    }

    public void setdataTempatTanggalLahir(String dataTempatTanggalLahir) {
        this.dataTempatTanggalLahir = dataTempatTanggalLahir;
    }
    public String getdataTempatTanggalLahir() {
        return dataTempatTanggalLahir;
    }
    
    public abstract double hitungIuran();

    @Override
    public String toString() {
        return "";
    }
    
}

